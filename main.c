#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "funciones.h"

int flag = 0, n = 0, i = 0, j = 0, num = 0, aux;
double promBebidas = 0, promConfites = 0, promConcervas = 0;
int Matriz[12][3], bebidas[12], confites[12], concervas[12];
void ingreso();
void promedioAnual();
void ordenarMatriz();
void mayorDulce();
void mayorBebida();
void menorDiciembre();
void buscarMes();
void buscarRubroMes();

int main()
{
    ingreso();
    promedioAnual();
    mayorDulce();
    mayorBebida();
    menorDiciembre();
    buscarMes();
    buscarRubroMes();
}
void ingreso()
{
    for (i = 1; i <= 12; i++)
    {
        for (j = 1; j <= 3; j++)
        {
            if (j == 1)
            {
                printf("ingrese el costo para el mes %d en bebidas \n", i);
                char cadena[6];
                fscanf(stdin, "%s", cadena);
                while (isNumber(cadena) == 1)
                {
                    printf("error!!, solo puedes ingresar numeros \n");
                    fscanf(stdin, "%s", cadena); // ingreso de datos
                }
                num = atoi(cadena);
                Matriz[i][j] = num;
            }
            if (j == 2)
            {
                printf("ingrese el costo para el mes %d en confites \n", i);
                char cadena[6];
                fscanf(stdin, "%s", cadena);
                while (isNumber(cadena) == 1)
                {
                    printf("error!!, solo puedes ingresar numeros \n");
                    fscanf(stdin, "%s", cadena); // ingreso de datos
                }
                num = atoi(cadena);
                Matriz[i][j] = num;
            }
            if (j == 3)
            {
                printf("ingrese el costo para el mes %d en concervas \n", i);
                char cadena[6];
                fscanf(stdin, "%s", cadena);
                while (isNumber(cadena) == 1)
                {
                    printf("error!!, solo puedes ingresar numeros \n");
                    fscanf(stdin, "%s", cadena); // ingreso de datos
                }
                num = atoi(cadena);
                Matriz[i][j] = num;
            }
        }
    }
}

void promedioAnual()
{
    int sum1 = 0, sum2 = 0, sum3 = 0;
    for (i = 1; i <= 12; i++)
    {
        for (j = 1; j <= 3; j++)
        {
            if (j == 1)
            {
                sum1 = sum1 + Matriz[i][j];
            }
            if (j == 2)
            {
                sum2 = sum2 + Matriz[i][j];
            }
            if (j == 3)
            {
                sum3 = sum3 + Matriz[i][j];
            }
        }
    }
    promBebidas = (sum1 / 12);
    promConfites = (sum2 / 12);
    promConcervas = (sum3 / 12);

    printf("\n-------------------\n");
    printf("el promedio de cortos de bebidas es de: %f \n", promBebidas);
    printf("el promedio de cortos de confites es de: %f \n", promConfites);
    printf("el promedio de cortos de conservas es de: %f \n", promConcervas);
    printf("\n-------------------\n");
    return;
}


void mayorDulce()
{
    int i, j, maxValor, IMax, JMax;
    maxValor = Matriz[1][2];
    for (i = 1; i <= 12; i++)
    {
        if (Matriz[i][2] > Matriz[i + 1][2])
        {
            maxValor = Matriz[i][2];
            IMax = i;
        }
    }
    printf("\n----------dulces---------\n");
    printf("el mes que mas costos tubo fue % d y el valor fue %d \n", IMax, maxValor);
    printf("\n-------------------\n");
    return;
}

void mayorBebida()
{
    int i, j, maxValor, IMax, minValor, IMin;
    maxValor = Matriz[1][2];
    for (i = 1; i <= 12; i++)
    {
        if (Matriz[i][2] > Matriz[i + 1][2])
        {
            maxValor = Matriz[i][2];
            IMax = i;
        }
    }
    minValor = Matriz[1][1];
    for (i = 1; i <= 12; i++)
    {
        if (Matriz[i][1] < Matriz[i + 1][1])
        {
            minValor = Matriz[i][1];
            IMin = i;
        }
    }
    printf("\n---------bebidas----------\n");
    printf("el mes que mas costos tubo fue % d y el valor fue %d \n", IMax, maxValor);
    printf("el mes que menos costos tubo fue % d y el valor fue %d \n", IMin, minValor);
    printf("\n-------------------\n");
    return;
}

void menorDiciembre()
{
    int i, maxValor, IMax, JMax;
    maxValor = Matriz[11][1];
    for (i = 1; i <= 12; i++)
    {
        if (Matriz[12][i] < Matriz[12][i + 1])
        {
            maxValor = Matriz[12][i];
            IMax = i;
        }
    }
    if (IMax == 1)
    {
        printf("\n----------diciembre---------\n");
        printf("el mes de diciembres el costo menor fue bebidas y el valor fue %d \n", maxValor);
        printf("\n-------------------\n");
    }
    if (IMax == 2)
    {
        printf("\n----------diciembre---------\n");
        printf("el mes de diciembres el costo menor fue confites y el valor fue %d \n", maxValor);
        printf("\n-------------------\n");
    }
    if (IMax == 3)
    {
        printf("\n----------diciembre---------\n");
        printf("el mes de diciembres el costo menor fue concervas y el valor fue %d \n", maxValor);
        printf("\n-------------------\n");
    }
    return;
}
// guncion que consulata el mes y devulve los valores para los 3 rubros
void buscarMes()
{
    printf("\n--------buscar por mes---------------\n");
    printf("ingrese el mes a consultar de 1 a 12 \n");
    char cadena[6];
    fscanf(stdin, "%s", cadena);
    while (isNumber(cadena) == 1)
    {
        printf("error!!, solo puedes ingresar numeros \n");
        fscanf(stdin, "%s", cadena); // ingreso de datos
    }
    num = atoi(cadena);

    printf("\n----------buscar por mes---------\n");
    printf("en el mes seleccionado %d , bebidas = %d, confites = %d, conservas = %d \n", num, Matriz[num][1],Matriz[num][2],Matriz[num][3]);
    printf("\n-------------------\n");
}
// guncion que consulata el mes y el rubro a buscar y los devulve
void buscarRubroMes()
{
    printf("\n-----------buscar por mes y rubro-----------\n");
    printf("ingrese el mes a consultar de 1 a 12 \n");
    char cadena[6];
    fscanf(stdin, "%s", cadena);
    while (isNumber(cadena) == 1)
    {
        printf("error!!, solo puedes ingresar numeros \n");
        fscanf(stdin, "%s", cadena); // ingreso de datos
    }
    i = atoi(cadena);
    printf("ingrese el numero de productro 1=bebidas, 2=confites, 3=concervas \n");
    fscanf(stdin, "%s", cadena);
    while (isNumber(cadena) == 1)
    {
        printf("error!!, solo puedes ingresar numeros \n");
        fscanf(stdin, "%s", cadena); // ingreso de datos
    }
    j = atoi(cadena);
    printf("\n----------buscar por mes y rubro---------\n");
    printf("en el mes seleccionado %d y para el producto seleccionado %d el valor es %d \n", i, j, Matriz[i][j]);
    printf("\n-------------------\n");
}