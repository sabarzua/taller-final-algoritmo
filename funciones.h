//
// Created by oplyt on 30-12-2018.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
int isNumber(char corrector[5]);

int isNumber(char corrector[5])
{
    int t = strlen(corrector);
    int validador = 0;
    int cont = 0;
    while (cont < t && validador == 0)
    {
        if (isdigit(corrector[cont]) != 0)
        {
            cont = cont + 1;
        }
        else
        {
            validador = 1;
        }
    }
    return validador;
}
