# Taller Práctico - Algoritmo y Estructura de datos

## Integrantes


| Nombre | rut | 
|---------------------|-----------|
| César Morales Armijo | 17662751-4    |
| Sebastián Abarzua Órdenes|     |



##  Introducción

Este repositorio fué creado con el fin de presentar  `# Taller Práctico - Algoritmo y Estructura de datos` 
La presentación se encuentra an ela raíz de este proyecto con el nombre `presentacion.pptx`

<br/>

##  Comandos de ejeción del programa


### Crear códifo c compilado

Una vez entrado al directorio del proyecto, compilar código, a continuación los comandos

```sh
$ cd path/of/your/c/files/proyect
$ gcc filename.c -o fileNameCompiled
```
<br/>

### Ejecutar archivo c *compilado*
Una vez teniendo ya el código compilado podremos ejecutar dicho archivo y ejecutarlo finalmente para realizar el uso de él

```sh
$ cd path/of/your/c/files/proyect
$ ./fileNameCompiled
```
<br/>



> **Note:**
> Este repositorio es privado .
